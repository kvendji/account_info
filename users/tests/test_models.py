from django.test import TestCase

from users.models import Note


class NoteModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Note.objects.create(model_name='test_name', action='test_action')

    def test_model_name_max_length(self):
        note = Note.objects.get(id=1)
        max_length = note._meta.get_field('model_name').max_length
        self.assertEquals(max_length, 255)

    def test_action_max_length(self):
        note = Note.objects.get(id=1)
        max_length = note._meta.get_field('action').max_length
        self.assertEquals(max_length, 255)

    def test_get_absolute_url(self):
        note = Note.objects.get(id=1)
        self.assertEquals(note.get_absolute_url(), '/log/request/1/')
