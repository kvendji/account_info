from django.test import TestCase, Client
from django.urls.base import reverse_lazy


class ViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_signup_page(self):
        response = self.client.get(reverse_lazy('signup'))
        self.assertEqual(response.status_code, 200)

    def test_index_page(self):
        response = self.client.get(reverse_lazy('registration'))
        self.assertEqual(response.status_code, 200)

    def test_home_page(self):
        response = self.client.get(reverse_lazy('account-info'))
        self.assertEqual(response.status_code, 200)
