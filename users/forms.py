from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.forms.fields import DateField

from .models import CustomUser
from django.conf import settings


class CustomUserCreationForm(UserCreationForm):

    class Meta:
        model = CustomUser
        fields = ('username', 'email')


class CustomUserChangeForm(forms.ModelForm):

    birthday = DateField(input_formats=settings.DATE_INPUT_FORMATS,
                         widget=forms.DateTimeInput(attrs={
                             'class': 'form-control datetimepicker-input',
                             'data-target': '#datetimepicker1'
                         })
                         )

    class Meta:
        model = CustomUser
        fields = (
            'first_name',
            'last_name',
            'birthday',
            'place_of_birth',
            'biography',
            'phone'
        )
