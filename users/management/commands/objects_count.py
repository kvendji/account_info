from django.core.management.base import BaseCommand

from users.models import CustomUser, LogRequest


class Command(BaseCommand):

    def handle(self, *args, **options):
        data = {
            'CustomUser': CustomUser.objects.all().count(),
            'LogRequest': LogRequest.objects.all().count()
        }
        print(data)
