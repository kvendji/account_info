from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, FormView
from django.views.generic.base import TemplateView

from .forms import CustomUserCreationForm, CustomUserChangeForm


class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('registration')
    template_name = 'registration/signup.html'


class AccountInfoView(TemplateView):
    template_name = 'account/home.html'


class ChangeInfoView(FormView):
    form_class = CustomUserChangeForm
    template_name = 'account/change_info.html'
    success_url = reverse_lazy('account-info')
