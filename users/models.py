from django.contrib.auth.models import AbstractUser
from django.db import models
from django.core.validators import RegexValidator
from django.db.models.signals import post_save, post_delete


class CustomUser(AbstractUser):
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be"
                                         " entered in the format: "
                                         "'+999999999'."
                                         " Up to 13 digits allowed.")
    birthday = models.DateField(null=True)
    place_of_birth = models.CharField(max_length=255, null=True)
    biography = models.TextField(null=True,
                                 blank=True)
    phone = models.CharField(validators=[phone_regex],
                             max_length=13,
                             null=True)

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return "/user/%i/" % self.id


class LogRequest(models.Model):
    total_time = models.CharField(max_length=255, null=True)
    path = models.CharField(max_length=255, null=True)
    method = models.CharField(max_length=30, null=True)
    user = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.path

    def get_absolute_url(self):
        return "/log/request/%i/" % self.id


class Note(models.Model):
    model_name = models.CharField(max_length=255, null=True)
    action = models.CharField(max_length=255, null=True)
    change_time = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return "/log/request/%i/" % self.id


def post_save_user(**kwargs):
    Note.objects.create(model_name='CustomUser', action='create')


def post_delete_user(instance, **kwargs):
    Note.objects.create(model_name='CustomUser', action='delete')


post_save.connect(post_save_user, sender=CustomUser)
post_delete.connect(post_delete_user, sender=CustomUser)
