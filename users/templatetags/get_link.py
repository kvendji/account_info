from django import template

register = template.Library()


@register.simple_tag
def get_link(object_of_model):
    return object_of_model.get_absolute_url()
