from django.urls import path

from .views import SignUpView, AccountInfoView, ChangeInfoView

urlpatterns = [
    path('info',  AccountInfoView.as_view(), name='account-info'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('change_info/', ChangeInfoView.as_view(), name='change_info'),
]
