from django.conf import settings


def some_settings(request):
    # return any value you want as a dictionary.
    return {
        'settings': settings,
    }
