import csv
import time

from users.models import LogRequest


def timing_middleware(get_response):
    def middleware(request):
        # Save request and time to file
        t1 = time.time()
        response = get_response(request)
        t2 = time.time()
        total = (t2 - t1)
        data = [
            ["TOTAL TIME:", total],
            ["Request path:", request.path],
            ["Request method:", request.method],
            ["Request user:", request.user if request.user else '-'],
        ]
        with open('request_log.csv', 'a') as logfile:
            writer = csv.writer(logfile)
            writer.writerow(data)

        # Save request and time to db
        LogRequest.objects.create(
            total_time=total, path=str(request.path),
            method=str(request.method), user=str(request.user)
        )
        return response
    return middleware
